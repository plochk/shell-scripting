#!/usr/bin/env bash

package_list_file="${HOME}/packages.txt"

if command -v apt &> /dev/null ; then
    sudo apt install $(grep -vE "^\s*#" ${package_list_file} | tr "\n" " ")
fi

if command -v emerge &> /dev/null ; then
    sudo emerge --ask $(grep -vE "^\s*#" ${package_list_file} | tr "\n" " ")
fi
