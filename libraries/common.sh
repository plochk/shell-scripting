# common.sh
# Global Usage: Symlink from ~/.local/lib/bash/common.sh

#****************************************#
#              common.sh                 #
#          written by plochk             #
#          September 13, 2021            #
#                                        #
#       Library with common functions    #
#****************************************#

#=============================================================================
# Error Codes

# Reserved: (https://tldp.org/LDP/abs/html/exitcodes.html#EXITCODESREF)
# 1   - Catchall for general errors
E_ERROR=1
# 2   - Misuse of shell builtins
# 126 - Command invoked cannot execute
# 127 - Command not found
# 128 - invalid argument for exit
# 128->165 - Fatal error signal "n"
# 130 - Script terminated by Control-C
# 255* - Exit status out of range

E_NOTROOT=87

#============================================================================
# Constants

ROOT_UID=0

# Colors
# use in echo with echo -e (enable backslash escapes)
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color, use after color to reset

SEPERATOR='
==============================================================================
'

#=============================================================================
# Functions

# $1: character; $2: many times
#------------------------------------------------#
# repeat_char()                                  #
# Repeat a char n-times                          #
# Parameters:                                    #
#   $1: char                                     #
#   $2: n                                        #
# Returns: 0                                     #
#------------------------------------------------#
repeat_char()
{
    result=""
    for i in $(seq "$2"); do result+="${1}"; done
    echo $result
    return 0
}


#------------------------------------------------#
# print_heading()                                #
# Prints a nice headline                         #
# Parameters:                                    #
#   $1: String                                   #
# Returns: 0                                     #
#------------------------------------------------#
print_heading()
{

    heading=$1
    heading_length=${#heading}

    result="${RED}#"
    result+=$(repeat_char "-" $(( 6 + heading_length )))
    result+="#\n"

    result+="#   ${1}   #\n"

    result+='#'
    result+=$(repeat_char "-" $(( 6 + heading_length )))
    result+="#${NC}"

    # change Internal Field Seperator to preserve whitespaces on stdout
    IFS='%'
    echo $result
    unset IFS
    return 0
}


#------------------------------------------------#
# check_is_root()                                #
# Checks if the script was run with sudo/as root #
# Parameters: None                               #
# Returns: 0 if root, $E_NOTROOT if not          #
#------------------------------------------------#
check_is_root()
{
    if [ "${UID}" -ne "${ROOT_UID}" ]; then
        echo "must be root to run this script."
        return ${E_NOTROOT}
    fi
    return 0
}


#------------------------------------------------#
# check_for_internet()                           #
# Test for an active internet connection         #
# Parameters: None                               #
# Returns: 0 on success, $E_ERROR on failure     #
#------------------------------------------------#
check_for_internet()
{
    # get website, without downloading (--spider)
    if wget -q --tries=10 --timeout=20 --spider https://www.duckduckgo.com > /dev/null;
    then
            return 0
    else
            return ${E_ERROR}
    fi
}
