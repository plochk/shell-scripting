"#"   # Comment
";;"  # Terminator in switch-case
";"   # Command Seperator in one line
"."   # if alone: alias to "source" command
      # if in path: current directory or hidden file
"\""  # String, weak
"\'"  # String, strong precedence
","   # Link series of arithmetic operators
"\`"  # Backtick, makes output of `command` available for variable
":"   # NOP, infinite loop: "while :..do..done"

