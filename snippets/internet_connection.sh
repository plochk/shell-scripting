# Check whether we have an internet connection
echo "Checking internet connectivity.."
/usr/bin/wget -q --tries=10 --timeout=20 --spider https://www.duckduckgo.com > /dev/null
#* get website, without downloading (spider) and delete it (/dev/null) 
if [[ $? -eq 0 ]]; then
        /bin/echo "done."
else
        /bin/echo "!! Error. No Connection."
        exit -1
fi
