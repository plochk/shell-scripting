#!/bin/sh

# Using a libraries function in your script
# Do not use "#!/bin/sh" with libraries as you dont want to spawn an extra
# shell
# you source them:
. ./library.sh

# Scoping of Variables
# Only $1-$9 are local, rest is global

# Example
add_a_user()
{
  USER=$1
  PASSWORD=$2
  shift; shift;
  # Having shifted twice, the rest is now comments ...
  COMMENTS=$@
  echo "Adding user $USER ..."
  echo useradd -c "$COMMENTS" $USER
  echo passwd $USER $PASSWORD
  echo "Added user $USER ($COMMENTS) with pass $PASSWORD"
}
