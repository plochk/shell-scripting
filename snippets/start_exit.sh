#!/bin/sh
# This translates to the default shell!

#=============================================================================
# Exit Codes
#
# Codes between 0-255, Success = 0, No Matches = 1, Errors = 2-255

exit 0
# only "exit": quit with last code

# Checking error codes cleanly: (source: shellscript.sh)
check_errs()
{
  # Function. Parameter 1 is the return code
  # Para. 2 is text to display on failure.
  if [ "${1}" -ne "0" ]; then
    echo "ERROR # ${1} : ${2}"
    # as a bonus, make our script exit with the right error code.
    exit ${1}
  fi
}
