#!/usr/bin/env bash

# WARNING: Errors on variables will be the empty '' string! e.g. misspelling

x="Hello World"
echo ${x}
echo $x

# Use `` to use another command inside string

# $0: basename of the program as it was called
# `basename $0`: only name of program file
# $1 - $9: first nine parameters
# $@ : all parameters
# $# : number of parameters
# $?: exit code of last command
# $!: PID of last run background process
# $$: PID of current shell

# Reading more than 9 arguments with shift:
while [ "$#" -gt "0" ]
do
  echo "\$1 is $1"
  shift
done

# Checking if last command succeded
/usr/local/bin/my-command
if [ "$?" -ne "0" ]; then
  echo "Sorry, we had a problem there!"
fi

# Default values for variables - ":-"
# Use:
echo "Your name is : ${myname:-`whoami`}"
# Instead of:
echo -en "What is your name [ `whoami` ] "
read myname
if [ -z "$myname" ]; then
  myname=`whoami`
fi
echo "Your name is : $myname"

