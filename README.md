Collection of shell scripts, custom libraries and snippets.

Use shellcheck.

Sources:
    [1] https://tldp.org/LDP/abs/html/index.html
    [2] https://www.shellscript.sh
