#!/usr/bin/env bash

# check if ~/.local/bin exists
bin_dir="${HOME}/.local/bin"
[ ! -d ${bin_dir} ] && \
    mkdir -p ${bin_dir}

# check if ~/.local/lib/bash exists
lib_dir="${HOME}/.local/lib/bash"
[ ! -d ${lib_dir} ] && \
    mkdir -p ${lib_dir}

# get this dir
update_script_location=$(pwd)

# symlink common library
[ ! -f "${HOME}/.local/lib/bash/common.sh" ] && \
    ln -s "${update_script_location}/libraries/common.sh" "${HOME}/.local/lib/bash/common.sh"

# symlink update script
[ ! -f "${HOME}/.local/bin/update" ] && \
ln -s "${update_script_location}/update.sh" "${HOME}/.local/bin/update"
