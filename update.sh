#!/usr/bin/env bash
# update system: packages and flatpak

source ~/.local/lib/bash/common.sh

# Check if we have an internet connection
if ! check_for_internet ; then
    echo -e "${RED}[-]${NC} No internet connection active! Aborting."
    exit ${E_ERROR}
fi

# zypper
if command -v zypper &> /dev/null ; then
    echo -e "\n$( print_heading 'zypper update' )"
    sudo zypper refresh
    sudo zypper update -y
    sudo zypper cc -a
fi

# apt
if command -v apt &> /dev/null ; then
    echo -e "\n$( print_heading 'apt update, upgrade & autoremove' )"
    sudo apt update -y
    sudo apt upgrade -y
    sudo apt autoremove -y
fi

if command -v flatpak &> /dev/null ; then
    echo -e "\n$( print_heading 'flatpak update' )"
    sudo flatpak update -y
fi


exit 0
